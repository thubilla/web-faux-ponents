const assert = require('assert');
const View = require('../src/lib/classes/view');

describe('View Class', function(){
	var component;
	before(function(){
		component = new View({
			id: 'component-view',
			parentNode: document.body,
			classList: ['component', 'demo', 'arbitrary'],
			data: {
				textContent: 'Hello World!'
			}
		});
	});

	it('takes config options and updates itself', function(){
		assert.equal(component.id, 'component-view');
	});

	it('has a render method which appends the component to its parentNode', function(){
		var $body = document.body;
		component.render();

		assert.ok($body.querySelector('#component-view'));
	});

	it('has an update method which replaces the component on its parentNode', function(){
		var $body = document.body,
			$el;

		component.render();

		$el = $body.querySelector('#component-view');
		assert.equal($el.textContent.trim(), 'Hello World!');
		
		component.data.textContent = 'Goodbye World!';
		component.update($el);

		assert.equal($el.textContent.trim(), 'Goodbye World!');
	});
});