const assert = require('assert');
var sayHello = require('../src/lib/util/hello');

describe('Say Hello', function() {
	it('with default parameters', function() {
		assert.equal(sayHello(), 'Hello World!');
	});

	it('with input parameters', function() {
		assert.equal(sayHello('Sherlock'), 'Hello Sherlock!');
	});
});