const express = require('express');
const cors = require('cors');
const faker = require('faker');

function api(limit){
	var data = {
		users: []
	};

	for(let i = 0; i < limit; i++){
		data.users.push({
			id: i,
			name: faker.name.findName(),
			avatar: faker.internet.avatar(),
			bio: faker.lorem.paragraphs(2)
		});
	}

	return data;
}

var app = express();

app.use(cors());

app.set('port', process.env.PORT || 3000);

app.use('/', express.static('./'));

app.get('/api/v1', function(req, res){
	var data = api(10);
	res.send(data);
});

app.delete('/api/v1/:id', function(req, res){
	res.write('success');
	res.end();
});

app.listen(app.get('port'), function(){
	var message = `
🔥

Express started & is listening on port ${app.get('port')}. 
Press Ctrl-C to terminate.

🔥
	`;
	
	console.log(message);
});
