/**
	Third Party Libraries
*/
const faker = require('faker');
const yo = require('yo-yo');
const css = require('sheetify');

/**
	Core Classes
*/
const Application = require('./lib/core');
const View = require('./lib/classes/view');

/**
	Utility Modules
*/
const devMode = require('./lib/util/livereload-scripttag');

class Demo extends View {
	constructor(config){
		super(config);
		return this;
	}

	html(){
		const prefix = css`
			:host {
				border: 1px solid mistyrose;
				padding: 1em;
			}

			:host h5{
				color: goldenrod;
			}
		`;

		return yo`<div id="${this.id}" class="${prefix}  ${this.classList.join(' ')}">
			<h5>${this.data.textContent}</h5>
		</div>`;
	}

}

window.onload = function(){
	const app = new Application();
	
	if(window && window.document){
		window.document.body.setAttribute('id', 'app');
	} else {
		return;
	}

	if(location && location.href.includes('localhost') || location.href.includes('192')){
		devMode();
		window.app = app;
	}
	
	const $container = document.querySelector('#container');
	const demo = new Demo({
		id: 'demo-view',
		parentNode: $container,
		classList: [ 'demo', 'view', 'fauxponent' ],
		data: {	
			textContent: 'This is a View Component built with Browserify'
		}
	});

	css('normalize-css/normalize.css');
	css('./app.css');
	demo.render();
};
