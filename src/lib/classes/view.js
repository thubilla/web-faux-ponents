// import yo from 'yo-yo';
// import update from '../util/update-this';

const yo = require('yo-yo');
const update = require('../util/update-this');

module.exports = class View {
	constructor(config){
		this.config = update;
		this.config(config);
		return this;
	}

	render(callback, ...args){
		const $el = this.html();

		if(this.parentNode.firstChild && this.parentNode.firstChild.id == this.id){
			yo.update(this.parentNode.firstChild, $el);
		} else {
			this.parentNode.appendChild($el);	
		}

		if(callback){
			callback(...args);
		}
	}

	update(target){
		console.log('updating');
		yo.update(target, this.html());
	}

	html(){
		return yo`<div id="${this.id}" class="${this.classList.join(' ')}">
			${this.data && this.data.textContent ? this.data.textContent : 'This is a View Component'}
		</div>`;
	}
}