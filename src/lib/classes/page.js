// import yo from 'yo-yo';
// import View from './view';

const yo = require('yo-yo');
const View = require('./view');

module.exports = class Page extends View {
	constructor(config){
		super(config);
		return this;
	}

	render(callback, ...args){
		if(this.parentNode.firstChild && this.parentNode.firstChild.classList.contains('page')){
			this.parentNode.removeChild(this.parentNode.querySelector('.page'));
		}

		var html = this.html(),
			footer = this.parentNode.querySelector('footer');

		this.parentNode.insertBefore(html, footer);

		if(callback){
			callback(...args);
		}
	}
}